$(document).ready(function(){
    load_date();
    load_user();
    function load_date(){
        $.ajax({
            url:"load_date.php",
            method:"POST",
            success:function(data)
            {
              $('.todolist').html(data);
            }
          });
    }
    function load_user(){
        $.ajax({
            url:"load_user.php",
            method:"POST",
            success:function(data)
            {
              $('.users').html(data);
            }
          });
    }
    //$(".todolist").hide();
    var i = 0 ;
    $(".addtask").click(function(){
        var name = $('.taskaddname').val();
        $.post("./add.php",
        {
          name: name
        },
        function(data, status){
          alert("Data: " + data + "\nStatus: " + status);
          load_date();
        });
    });
    
    $(".openlist").click(function(){
        $(".todolist").toggle(1000);
    });
    $(".openuser").click(function(){
        $(".users").toggle(1000);
    });
    $(".edit").click(function(){
        $(".infoblok").toggle();
    });
});