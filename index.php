<?php
    require 'db.php'; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/style/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="contentw">
    <div class="addtodo">
            <form >
                <input type = 'text' class="taskaddname">
                <button type="button" class="addtask" >ADD + </button>
            </form>
        </div>
        <div class="open">
            <p class="openlist">To do list</p>
        </div>
        <div class = "todolist">    
        </div>
        <div class="open">
            <p class="openuser">Users</p>
        </div>
        <div class = "users">     
        </div>  
    </div>
</body>
<footer>
    <script src = "js/jquery.js"></script>
</footer>
</html>