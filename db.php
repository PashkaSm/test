<?php 
$sName = "localhost";
$uname = "root";
$pass = "";
$db_name = "test";
try{
    $conn = new PDO ("mysql:host=$sName;dbname=$db_name",$uname,$pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    
}catch(PDOException $e){
    echo "No connection: ". $e->getMessage(); 
}